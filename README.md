# Kiranraj's blog

A demo blog app.

## Screenshot
![](demo.gif)

## Usage

Make sure you have Flutter installed on your local machine. For more instructions on how to install flutter, look [here](https://flutter.io/docs/get-started/install).
```
git clone https://gitlab.com/kiranraj2k/sample-blog.git
cd sample-blog
flutter run
```

## Structure

The main modules are under `lib` directory.

directory | description
:--: | :--
dummy_data | Holds the dummy data which is used to display the articles.
blocs | Classes which handles the data(which are consumed from providers) updation in the UI making use of dart streams.
models | Model classes which are used to model data.
providers | Classes that act as a data storage(or to store data).
screens | Classes to show each of the pages(or views).
widgets | Classes(or widgets) that are reused.

## What's next?

 - [ ] Improve design of article description in the article content page 
 - [ ] Make the app notch-friendly
 - [ ] Persist the bookmark data which will make the bookmarks available after reopening the app 
 - [ ] Add app logo
 - [ ] Rename app name
 - [ ] Make the app tablet-friendly
 - [ ] View bookmarked list
 - [ ] The code for the bookmark icon could be reused i.e to add it to widgets.

## Credits

  - UI/UX inspired from [**Medium**](https://medium.com/) site.
  - The dummy data(including images) was obtained from [**Aeon**](https://aeon.co/) and [**Quanta Magazine**](https://www.quantamagazine.org/).
