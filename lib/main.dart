import 'package:flutter/material.dart';
import 'package:sample_blog/screens/screens.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      onGenerateRoute: (settings) {
        WidgetBuilder builder;

        switch (settings.name) {
          case '/':
            builder = (
              BuildContext _,
            ) =>
                ArticleList();
            break;
          case '/article_content':
            builder = (
              BuildContext _,
            ) =>
                ArticleContent(
                  article: settings.arguments,
                );
            break;
          default:
            return null;
        }

        return MaterialPageRoute(builder: builder, settings: settings);
      },
    );
  }
}
