import 'package:flutter/material.dart';
import 'package:sample_blog/blocs/bookmarks.dart';
import 'package:sample_blog/providers/bookmarks.dart';
import 'package:sample_blog/models/article.dart';
import 'package:sample_blog/widgets/article_item.dart';
import 'package:sample_blog/dummy_data/dummy_data.dart';

class ArticleList extends StatefulWidget {
  @override
  _ArticleListState createState() => _ArticleListState();
}

class _ArticleListState extends State<ArticleList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: StreamBuilder(
        initialData: [],
        stream: bookmarkBloc.getBookmarks,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Colors.black,
              title: Text("Kiranraj's blog"),
              actions: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                    right: 10,
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "${bookmarkProvider.bookmarks.length} ",
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                      Icon(Icons.collections_bookmark),
                    ],
                  ),
                ),
              ],
            ),
            body: ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                Map articleData = dummyBlog[index];

                return ArticleItem(
                  article: Article(
                    index,
                    articleData["title"],
                    articleData["description"],
                    articleData["image"],
                  ),
                );
              },
              itemCount: dummyBlog.length,
            ),
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    bookmarkBloc.dispose();

    super.dispose();
  }
}
