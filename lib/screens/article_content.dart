import 'package:flutter/material.dart';
import 'package:sample_blog/models/article.dart';
import 'package:sample_blog/blocs/bookmarks.dart';
import 'package:sample_blog/providers/bookmarks.dart';

class ArticleContent extends StatefulWidget {
  final Article article;

  const ArticleContent({Key key, this.article}) : super(key: key);

  @override
  _ArticleContentState createState() => _ArticleContentState();
}

class _ArticleContentState extends State<ArticleContent> {
  Article article;
  bool bookmarked = false;

  @override
  Widget build(BuildContext context) {
    article = widget.article;

    Widget header = SliverAppBar(
      primary: true,
      floating: false,
      snap: false,
      pinned: true,
      backgroundColor: Colors.black,
      expandedHeight: 200.0,
      leading: IconButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        icon: Icon(
          Icons.arrow_back,
          color: Colors.white,
          size: 30.0,
        ),
      ),
      flexibleSpace: FlexibleSpaceBar(
        background: Hero(
          tag: "image${article.id}",
          child: Image.asset(
            article.image,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );

    Widget title = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Flexible(
          child: Padding(
            padding: EdgeInsets.only(
              top: 20.0,
              left: 10.0,
              right: 10.0,
            ),
            child: Text(
              article.title,
              style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ],
    );

    Widget bookmark = StreamBuilder(
      stream: bookmarkBloc.getBookmarks,
      builder: (context, snapshot) {
        bookmarked = bookmarkProvider.getIndexOfItem(article) >= 0;

        return Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Hero(
              tag: "bookmark${article.id}",
              child: Material(
                child: IconButton(
                  onPressed: () {
                    bookmarkBloc.toggle(article);
                  },
                  icon:
                      Icon(bookmarked ? Icons.bookmark : Icons.bookmark_border),
                ),
              ),
            ),
          ],
        );
      },
    );

    Widget description = Padding(
      padding: EdgeInsets.only(
        left: 10.0,
        right: 10.0,
      ),
      child: Text(
        article.description,
        textAlign: TextAlign.left,
        style: TextStyle(
          height: 1.0,
          fontSize: 17,
        ),
      ),
    );

    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return [
            header,
          ];
        },
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              title,
              bookmark,
              description,
            ],
          ),
        ),
      ),
    );
  }
}
