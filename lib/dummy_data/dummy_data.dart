List dummyBlog = [
  {
    "title": "Why Spinoza still matters?",
    "description":
        "In July 1656, the 23-year-old Bento de Spinoza was excommunicated from the Portuguese-Jewish congregation of Amsterdam. It was the harshest punishment of herem (ban) ever issued by that community. The extant document, a lengthy and vitriolic diatribe, refers to the young man’s ‘abominable heresies’ and ‘monstrous deeds’. The leaders of the community, having consulted with the rabbis and using Spinoza’s Hebrew name, proclaim that they hereby ‘expel, excommunicate, curse, and damn Baruch de Spinoza’. He is to be ‘cast out from all the tribes of Israel’ and his name is to be ‘blotted out from under heaven’",
    "image": "images/spinoza.jpg",
  },
  {
    "title": "Time to update the Nobels",
    "description":
        "Imagine the outcry if, at the 2016 Summer Olympics, the legendary United States swim team –​ Michael Phelps, Ryan Lochte, Conor Dwyer and Townley Haas –​ still obliterated the competition, coming first in the men’s 4 x 200m freestyle relay, but only Haas, Lochte and Dwyer received medals, with nothing, not even a silver, for Phelps. ‘Unfair!’ you’d cry. And you’d be right. The Nobel committee seems not to recognise how collaborative science is today; their paradigm remains the lone genius, or a duet or troika at most. Year after year, they perform their arbitrary and often cruel calculus, leaving deserving physicists shivering in the pool without any medal to show for it. Even those few modern experimentalists who have won unshared Nobel prizes owe their success to numerous collaborators – especially​ in particle physics and astronomy, which require massive data sets and large teams to analyse them. No scientist gets to Stockholm alone. ",
    "image": "images/nobel.jpg",
  },
  {
    "title": "Is acting hazardous?",
    "description":
        "In 2009, Heath Ledger posthumously received an Academy Award for his performance as the Joker in Christopher Nolan’s film The Dark Knight (2008). To say that Ledger earned the recognition of his peers is to vastly understate his accomplishment. Ledger’s unflinching and disquieting performance as an anarchic sociopath – ostensibly, he played a comic-book villain, but his performance far transcended the source material – earned near-universal praise from critics and audiences alike. By the time filming wrapped up, Ledger had completed his professional transition from ingénu to serious actor. As his final director, Terry Gilliam, remarked: ‘I think we all thought that this was somebody, without a doubt, who was going to be the greatest actor of his generation.’",
    "image": "images/joker.jpg",
  },
  {
    "title": "Cheers! How the physics of fizz contributes to human happiness",
    "description":
        "Think of the last time you had something to celebrate. If you toasted the happy occasion, your drink was probably alcoholic – and bubbly. Have you ever wondered why it’s so enjoyable to imbibe a glass of something that sets off a series of microexplosions in your mouth? ",
    "image": "images/physics.jpg",
  },
  {
    "title": "Cognitive gadgets",
    "description":
        "Bookshops are wonderful places – and not all the good stuff is in books. A few months ago, I spotted a man standing in the philosophy section of a local bookshop with his daughter, aged three or four. Dad was nose-deep in a tome, and his daughter was taking care of herself. But rather than wreaking havoc with the genres or scribbling on a flyleaf, she was doing exactly as her father was: with the same furrowed brow, bowed posture and chin-stroking fingers, this small child was gazing intently at a book of mathematical logic.",
    "image": "images/books.jpg",
  },
  {
    "title": "An electrical meltdown looms: how can we avert disaster?",
    "description":
        "You might find your car dying on the freeway while other vehicles around you lose control and crash. You might see the lights going out in your city, or glimpse an airplane falling out of the sky. You’ve been in a blackout before but this one is different.",
    "image": "images/electricity.jpg",
  },
  {
    "title": "In defence of disorder",
    "description":
        "The Buddhist monks from Namgyal monastery in India engage in a ritual that involves the creation of intricate patterns of coloured sand, known as mandalas. As large as three metres across, each mandala requires a couple of weeks of painstaking work, in which several monks in orange robes bend over a flat surface and scratch metallic vials. The vials extrude sand from tiny spouts, a few grains at a time, onto areas bounded by carefully measured chalk marks. Slowly, slowly, the ancient pattern is made. After the thing is completed, the monks say a prayer, pause a moment, and then sweep it all up in five minutes.",
    "image": "images/disorder.jpg",
  },
  {
    "title": "Medieval parasites",
    "description":
        "In the film Monty Python and the Holy Grail (1975), two minor characters spot King Arthur. They know who he is because, as one of them points out: ‘He must be a king … he hasn’t got shit all over him like the rest of us.’ The scene encapsulates an enduring belief about the Middle Ages: medieval people were dirty. Some might have heard Elizabeth I’s famous (but probably apocryphal) declaration that she had a bath once a month whether she needed it or not. In a time when only the richest enjoyed running water in their homes, very few Europeans had the resources to abide by 21st-century standards of hygiene, even if they wanted to.",
    "image": "images/medievel.jpg",
  },
  {
    "title": "Why hasn’t evolution dealt with the inefficiency of ageing?",
    "description":
        "Life pits the order and intricacy of biology against the ceaseless chaos of physics. The second law of thermodynamics, or the thermodynamic arrow of time, states that any natural system will always tend towards increasing disorder. Biological ageing is no different, making death inevitable. However, one of the least-addressed questions of ageing is the apparent paradox between the optimising drive of evolution, and the inevitable deterioration of the body. Considering the 3.5 billion years in which we have evolved from single-cell organisms, why hasn’t life countered the inefficiency of ageing? Or more accurately, how has ageing persisted within the Darwinian framework of evolution? ",
    "image": "images/evolution.jpg",
  },
  {
    "title": "A World Without Clouds",
    "description":
        "On a 1987 voyage to the Antarctic, the paleoceanographer James Kennett and his crew dropped anchor in the Weddell Sea, drilled into the seabed, and extracted a vertical cylinder of sediment. In an inch-thick layer of plankton fossils and other detritus buried more than 500 feet deep, they found a disturbing clue about the planet’s past that could spell disaster for the future.",
    "image": "images/clouds.jpg",
  }
];
