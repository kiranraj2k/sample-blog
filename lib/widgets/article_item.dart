import "package:flutter/material.dart";
import 'package:sample_blog/models/article.dart';
import 'package:sample_blog/providers/bookmarks.dart';
import 'package:sample_blog/blocs/bookmarks.dart';

class ArticleItem extends StatefulWidget {
  const ArticleItem({Key key, this.article}) : super(key: key);

  final Article article;

  @override
  _ArticleItemState createState() => _ArticleItemState();
}

class _ArticleItemState extends State<ArticleItem> {
  bool bookmarked = false;
  Article article;

  @override
  Widget build(BuildContext context) {
    article = widget.article;
    bookmarked = bookmarkProvider.getIndexOfItem(article) >= 0;

    return ListTile(
      leading: Hero(
        tag: "image${article.id}",
        child: Container(
          width: 45,
          height: 45,
          decoration: BoxDecoration(
            color: Colors.black,
            image: DecorationImage(
              image: ExactAssetImage(article.image),
              fit: BoxFit.fill,
            ),
          ),
        ),
      ),
      onTap: () {
        Navigator.pushNamed(
          context,
          '/article_content',
          arguments: article,
        );
      },
      title: Text(
        article.title,
        style: TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
      isThreeLine: true,
      subtitle: Text(
        article.description,
        maxLines: 3,
        overflow: TextOverflow.ellipsis,
      ),
      trailing: Hero(
        tag: "bookmark${article.id}",
        child: Material(
          child: IconButton(
            icon: Icon(
              bookmarked ? Icons.bookmark : Icons.bookmark_border,
              color: Colors.black,
            ),
            onPressed: () => bookmarkBloc.toggle(article),
          ),
        ),
      ),
    );
  }
}
