class Article {
  final int id;
  final String title;
  final String description;
  final String image;

  Article(this.id, this.title, this.description, this.image);

  Article.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        title = json['title'],
        description = json['description'],
        image = json['image'];
}
