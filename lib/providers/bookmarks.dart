import "package:sample_blog/models/article.dart";

class BookmarkProvider {
  List<Article> bookmarks = [];

  int getIndexOfItem(Article item) {
    return bookmarks.indexWhere(
      (article) {
        return article.id == item.id;
      },
    );
  }

  void toggle(article) {
    int index = getIndexOfItem(article);

    if (index >= 0) {
      bookmarks.removeAt(index);
    } else {
      bookmarks.add(article);
    }
  }
}

final bookmarkProvider = BookmarkProvider();
