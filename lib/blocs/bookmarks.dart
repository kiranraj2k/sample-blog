import 'dart:async';
import 'package:sample_blog/providers/bookmarks.dart';

class BookmarkBloc {
  final bookmarkController = StreamController.broadcast();
  Stream get getBookmarks => bookmarkController.stream;

  void addToSink() {
    bookmarkController.sink.add(bookmarkProvider.bookmarks);
  }

  void toggle(article) {
    bookmarkProvider.toggle(article);

    addToSink();
  }

  void dispose() {
    bookmarkController.close();
  }
}

final bookmarkBloc = BookmarkBloc();
